﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
public class SokobanGameManager : MonoBehaviour
{
    Nivel nivel, nivelAux; // nivel
    GameObject casillero, casilleroTarget, pared, jugador, bloque,pisomalo; //los prefabs
    public List<Vector2> posOcupadasEsperadasCasillerosTarget;
    Stack pilaTablerosAnteriores = new Stack();

    string orientacionJugador;
    string nombreNivelActual = "Nivel2";
    //string nombreNivelActual = "Nivel2";
    bool gameOver = false;
    bool estoyDeshaciendo = false;

    // test
    //level manager
    //public Texture2D mapa;
    //public ColorPrefab[] colormappings;

    private void Start()
    {
        casillero = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "Casillero");
        casilleroTarget = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "CasilleroTarget");
        pared = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "Pared");
        jugador = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "Jugador");
        bloque = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "Bloque");
        pisomalo = SokobanLevelManager.instancia.dameLstPrefabsSokoban().Find(x => x.name == "Pisomalo");
        CargarNivel(nombreNivelActual);

        //TestInstancia();
    }

    private void CargarNivel(string nombre)
    {
        nivel = SokobanLevelManager.instancia.dameNivel(nombre);
        posOcupadasEsperadasCasillerosTarget = nivel.Tablero.damePosicionesObjetos("CasilleroTarget");
        //InstanciadorPrefabs.instancia.graficarCasilleros(nivel.Tablero, casillero);
        //InstanciadorPrefabs.instancia.graficarCasillerosTarget(nivel.Tablero, casilleroTarget);
        //InstanciadorPrefabs.instancia.graficarObjetosTablero(nivel.Tablero, SokobanLevelManager.instancia.dameLstPrefabsSokoban());
        for (int i = 0; i < SokobanLevelManager.instancia.mapa.width; i++)
        {
            for (int j = 0; j < SokobanLevelManager.instancia.mapa.height; j++)
            {

                InstanciadorPrefabs.instancia.graficarObjetosImagen(i, j, SokobanLevelManager.instancia.mapa, SokobanLevelManager.instancia.colormappings);
                
            }
        }
        InstanciadorPrefabs.instancia.graficarObjetosTablero(nivel.Tablero, SokobanLevelManager.instancia.dameLstPrefabsSokoban());
    }

    private void Update()
    {
        if (!gameOver)
        {
            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                orientacionJugador = "derecha";
                mover();
            }
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                orientacionJugador = "arriba";
                mover();
            }
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                orientacionJugador = "izquierda";
                mover();
            }
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                orientacionJugador = "abajo";
                mover();
            }
            if (Input.GetKeyDown(KeyCode.Z))
            {
                estoyDeshaciendo = true;
                mover();
            }
        }
       
         if (ChequearVictoria(nivel.Tablero))
            {
                Debug.Log("Gané");
                gameOver = true;
            }
    }

    private void mover()
    {

        //gameOver = ChequearVictoria(nivel.Tablero);
        if (estoyDeshaciendo == false)
        {

            Tablero tablAux = new Tablero(nivel.Tablero.casilleros.GetLength(0), nivel.Tablero.casilleros.GetLength(1));

            tablAux.setearObjetos(casillero, nivel.Tablero.damePosicionesObjetos("Casillero"));
            tablAux.setearObjetos(casilleroTarget, nivel.Tablero.damePosicionesObjetos("CasilleroTarget"));
            tablAux.setearObjetos(bloque, nivel.Tablero.damePosicionesObjetos("Bloque"));
            tablAux.setearObjetos(pared, nivel.Tablero.damePosicionesObjetos("Pared"));
            tablAux.setearObjetos(jugador, nivel.Tablero.damePosicionesObjetos("Jugador"));
            tablAux.setearObjetos(pisomalo, nivel.Tablero.damePosicionesObjetos("Pisomalo"));


            // punto 4 deshacer movimientos pulsando la Z, debemos utilizar un Stack 
            // pop es delete y push es add

            Vector2 posicionJugador = new Vector2(nivel.Tablero.damePosicionObjeto("Jugador").x, nivel.Tablero.damePosicionObjeto("Jugador").y);
            GameObject objActual, objProximo, objProximoProximo;
            objActual = nivel.Tablero.dameObjeto(posicionJugador, orientacionJugador, 0);
            objProximo = nivel.Tablero.dameObjeto(posicionJugador, orientacionJugador, 1);
            objProximoProximo = nivel.Tablero.dameObjeto(posicionJugador, orientacionJugador, 2);

           
            if (objProximo != null && objProximo.CompareTag("casillero"))
            {
                nivel.Tablero.setearObjeto(casillero, posicionJugador);
                nivel.Tablero.setearObjeto(jugador, posicionJugador, orientacionJugador, 1);
                pilaTablerosAnteriores.Push(tablAux);
            }
            else
            {
                if (objProximo != null && objProximo.CompareTag("bloque") && objProximoProximo != null && (!objProximoProximo.CompareTag("bloque") && objProximoProximo.tag != "pared"))
                {
                    nivel.Tablero.setearObjeto(jugador, posicionJugador, orientacionJugador, 1);
                    {
                        nivel.Tablero.setearObjeto(casillero, posicionJugador);
                        nivel.Tablero.setearObjeto(bloque, posicionJugador, orientacionJugador, 2); ;
                        pilaTablerosAnteriores.Push(tablAux);

                    }

                }
            }
            InstanciadorPrefabs.instancia.graficarObjetosTablero(nivel.Tablero, SokobanLevelManager.instancia.dameLstPrefabsSokoban());
           
        }
        else 
        {
            if (pilaTablerosAnteriores.Count > 0)
            {
                Tablero tablero = (Tablero)pilaTablerosAnteriores.Pop();


                nivel.Tablero.setearObjetos(casillero, tablero.damePosicionesObjetos("Casillero"));
                nivel.Tablero.setearObjetos(casilleroTarget, tablero.damePosicionesObjetos("CasilleroDash"));
                nivel.Tablero.setearObjetos(casilleroTarget, tablero.damePosicionesObjetos("CasilleroTarget"));
                nivel.Tablero.setearObjetos(bloque, tablero.damePosicionesObjetos("Bloque"));
                nivel.Tablero.setearObjetos(pared, tablero.damePosicionesObjetos("Pared"));
                nivel.Tablero.setearObjetos(jugador, tablero.damePosicionesObjetos("Jugador"));
                nivel.Tablero.setearObjetos(pisomalo, tablero.damePosicionesObjetos("Pisomalo"));
                InstanciadorPrefabs.instancia.graficarObjetosTablero(tablero, SokobanLevelManager.instancia.dameLstPrefabsSokoban());

            }
            else
            {
                InstanciadorPrefabs.instancia.graficarObjetosTablero(nivel.Tablero, SokobanLevelManager.instancia.dameLstPrefabsSokoban());
            }







            estoyDeshaciendo = false;



        }
    }

    private bool SonIgualesLosVectores(Vector2 v1, Vector2 v2)
    {
        return (v1.x == v2.x && v1.y == v2.y);
    }


    private bool ChequearVictoria(Tablero tablero)
    {
        int cont = 0;
        List<Vector2> PosBloc = tablero.damePosicionesObjetos("Bloque");


        foreach (Vector2 item in PosBloc) //para cada algo(vector2 item) que tenga en una lista
        {
            if (SonIgualesLosVectores(item, posOcupadasEsperadasCasillerosTarget[0]) || SonIgualesLosVectores(item,posOcupadasEsperadasCasillerosTarget[1]) || SonIgualesLosVectores(item, posOcupadasEsperadasCasillerosTarget[2]))
            {
                cont++;
            }
        }
        if (cont == 3)
        {
            Debug.Log("Ganaste");
            return true;
        }
        else
        {
            cont = 0;
            return false;
            
        }
       
    }

    
}

